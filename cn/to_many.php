<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <title>一对多资管产品</title>
    <link rel="shortcut icon" type="image/x-icon" href="../images/favicon.ico">
    <link rel="stylesheet" type="text/css" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" href="../css/animate.min.css" />
    <link rel="stylesheet" type="text/css" href="../css/swiper.min.css" /> 
    <link rel="stylesheet" type="text/css" href="../css/com.css" />    
    <style type="text/css">
         /*头部样式*/
        .onone header {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 3.42rem;
            text-align: center;
            font-size: 1.5rem;
            line-height: 3.42rem;
            color: #fff;
            background: #c60001;
            box-shadow: 0.1rem 1rem 1.5rem 0 #dadada;
            z-index: 99;
        }
        .onone header a {
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            width: 15%;
            height: 3.42rem;
            background: url(../images/arrowL.png) 0 0 no-repeat;
            background-size: contain;
        }
        .onone .swiper-container {
            padding-top: 3.42rem;
        }
        .onone .swiper-slide img {
            width: 100%;
            height: 100%;
        }
        /*底部导航模块*/
        .onone .footer {
            position: fixed;
            bottom: 0;
            z-index: 2;
            width: 100%;
            max-width: 640px;
            height: 4.83rem;
            background: #f2f2f2;
            border: 1px solid #e0e0e0;

        }
        .onone .footer a {
            float: left;
            width: 25%;
            height: 1.83rem;
            padding-top: 3rem;
            text-align: center;
            color: #3d3d3d;
        }
        .onone .footer a:nth-child(1) {
            background: url("../images/footer-1.png") no-repeat center 0.58rem;
            background-size: 30%;
        }
        .onone .footer a:nth-child(2) {
            background: url("../images/footer-2.png") no-repeat center 0.58rem;
            background-size: 35%;   
        }
        .onone .footer a:nth-child(3) {
            background: url("../images/footer-3.png") no-repeat center 0.58rem;
            background-size: 40%;   
        }
        .onone .footer a:nth-child(4) {
            background: url("../images/footer-4.png") no-repeat center 0.58rem;
            background-size: 29%;   
        }
    </style>
</head>
<body>
    <div class="onone">
        <header>
            一对多资管产品
            <a href="info_management.php"></a>
        </header>
        <!-- 大图滚动 -->
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src="../images/demo/fund13.png" alt=""></div>
                <div class="swiper-slide"><img src="../images/demo/fund12.png" alt=""></div>
                <div class="swiper-slide"><img src="../images/demo/fund11.png" alt=""></div>
            </div>
            <div class="swiper-pagination "></div>
        </div>
        <table class="date-data">
            <thead>
                <tr>
                    <th>产品名称</th>
                    <th>近7天</th>
                    <th>近1月</th>
                    <th>近3月</th>
                </tr>     
            </thead>
            <tbody>
                <tr>
                    <td class="beyond">吉科1号</td>
                    <td>6.058%</td>
                    <td>26.08%</td>
                    <td>61.0%</td>
                </tr>
                <tr>
                    <td class="beyond">万达资管</td>
                    <td>6.058%</td>
                    <td>26.08%</td>
                    <td class="negative">-11.0%</td>
                </tr>   
                <tr>
                    <td class="beyond">旭日升</td>
                    <td>6.058%</td>
                    <td class="negative">-6.08%</td>
                    <td>61.0%</td>
                </tr> 
                <tr>
                    <td class="beyond">班若资管</td>
                    <td>6.058%</td>
                    <td>26.08%</td>
                    <td>61.0%</td>
                </tr>        
            </tbody>
        </table>
         <footer class="footer">
                <a href="fund.php">基金</a>
                <a href="info_management.php">资管</a>
                <a href="news_list.php">研发资讯</a>
                <a href="online_service.php">在线客服</a>
        </footer>
    </div>
    <script type="text/javascript" src="../js/zepto.min.js"></script>
    <script type="text/javascript" src="../js/swiper.min.js"></script>
    <script type="text/javascript" src="../js/animate.min.js"></script>
    <script type="text/javascript">
        // 大图滚动swiper
        var mySwiper = new Swiper ('.swiper-container', {
            direction: 'horizontal',
            loop: true,
            autoplay : 3000,
            speed: 1000,
    
             // 如果需要分页器
            pagination: '.swiper-pagination',
        }) 
    </script>
</body>
</html>