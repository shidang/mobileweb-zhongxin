<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>资 管</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/swiper.min.css">
	<link rel="stylesheet" href="../css/animate.min.css">
	<link rel="stylesheet" href="../css/info_management.css">

</head>
<body>
	<div class="info_management">
		<!-- 基金首页头部 -->
		<header class="info_management-head">
			<a href="index.php"></a>
			<h1>资 管</h1>
		</header>
		<section>
		<!-- 基金大图滚动和内容模块 -->
			<div class="info_management-pic">
			<!-- 大图滚动 -->
				<div class="swiper-container">
				    <div class="swiper-wrapper">
				        <div class="swiper-slide"><img src="../images/demo/fund13.png" alt=""></div>
				        <div class="swiper-slide"><img src="../images/demo/fund12.png" alt=""></div>
				        <div class="swiper-slide"><img src="../images/demo/fund11.png" alt=""></div>
				    </div>
			    	<div class="swiper-pagination "></div>
				</div>
			</div>
			<!-- 内容模块 -->
			<div class="info_management-main clearfix">
				<ul>
					<li> <a href="fangda_info.php"></a>热销资管 </li>
					<li> <a href="on_one.php"></a>一对一资管 </li>
					<li> <a href="to_many.php"></a>一对多资管</li>
					<li> <a href="management_info.php"></a>资管资讯 </li>
					
				</ul>
			</div>
		</section>
		<!-- 底部导航 -->
		<footer class="footer">
				<a href="fund.php">基金</a>
				<a href="info_management.php">资管</a>
				<a href="news_list.php">研发资讯</a>
				<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/touch.js"></script>
	<script src="../js/common.js"></script>
	<script src="../js/swiper.min.js"></script>
	<script src="../js/fx.js"></script>
	<script src="../js/animate.min.js"></script>

	<script type="text/javascript">
		// 大图滚动swiper
		var mySwiper = new Swiper ('.swiper-container', {
    		direction: 'horizontal',
    		loop: true,
    		autoplay : 3000,
    
   			 // 如果需要分页器
   		 	pagination: '.swiper-pagination',
  		})  
	
	</script>
</body>
</html>