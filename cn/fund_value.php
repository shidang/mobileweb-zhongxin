<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>基金详情页</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/com.css">

</head>
<body>
	<div class="fund-value">
		<!-- 基金净值头部 -->
		<header class="fund-head">
			<a href="fund_hot.php"></a>
			<h1>
				<strong>汇添富民营活力</strong>
				<span>470009</span>
			</h1>
			<a href=""></a>
		</header>
		<section>
		<!-- 内容模块 -->
			<!-- 内容导航模块 -->
			<nav class="value-nav">
				<ul class="clearfix">
					<li> <a href="fund_details.php">基金概况</a></li>
					<li> <a href="fund_up_down.php">涨跌幅</a></li>
					<li> <a href="fund_value.php">基金净值</a></li>
					<li> <a href="fund_rate.php">基金费率 </a></li>
				</ul>
			</nav>
			<div class="value-pic">
				<img src="../images/demo/fund14.png" alt="">
			</div>
			<table class="date-data">
	            <thead>
	                <tr>
	                    <th>日 期</th>
	                    <th>单位净值</th>
	                    <th>累计净值</th>
	                </tr>     
	            </thead>
	            <tbody>
	                
	                 <tr>
	                    <td>2014.10.10</td>
	                    <td>1.005</td>
	                    <td ><span class="res">0.54</span></td>
	                </tr>
	               <tr>
	                    <td>2014.10.10</td>
	                    <td>1.005</td>
	                    <td ><span class="res">0.54</span></td>
	                </tr>
	                 <tr>
	                    <td>2014.10.10</td>
	                    <td>1.005</td>
	                    <td ><span class="res">0.54</span></td>
	                </tr>
	                 <tr>
	                    <td>2014.10.10</td>
	                    <td>1.005</td>
	                    <td ><span class="res">0.54</span></td>
	                </tr>
	            </tbody>
	        </table>
		</section>
		<!-- 底部导航 -->
		<footer class="footer">
				<a href="fund.php">基金</a>
				<a href="info_management.php">资管</a>
				<a href="news_list.php">研发资讯</a>
				<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/touch.js"></script>
	<script src="../js/common.js"></script>
	<script src="../js/fx.js"></script>
	 
</body>
</html>