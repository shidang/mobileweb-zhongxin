<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>基金详情页</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/com.css">


</head>
<body>
	<div class="fund-me">
		<!-- 基金净值头部 -->
		<header class="fund-head">
			<a href="fund.php"></a>
			<h1>
				我的基金
			</h1>
			<a href=""></a>
		</header>
				
		<section>
		<!-- 内容模块 -->
			<!-- 内容导航模块 -->
			<nav class="me-nav">
				<input type="text" value="输入股票名称/代码添加自选股">
				<ul>
					<li><a href="">全部</a></li>
					<li><a href="">股票</a></li>
					<li><a href="">混合</a></li>
					<li><a href="">债券</a></li>
					<li><a href="">更多</a></li>
				</ul>
			</nav>
			<table class="date-data">
	            <thead>
	                <tr>
	                    <th >基金名称</th>
	                    <th>最新净值</th>
	                    <th class="head-con">日涨跌幅</th>
	                </tr>     
	            </thead>
	            <tbody>
	                
	                <tr>
	                    <td>
		                    <em>中邮核心优势</em>590003
	                    </td>
	                    <td>
	                    	<i>1.3800</i>
	                    	2014.10.16
	                    </td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	               	<tr>
	                    <td><em>中邮核心优势</em>590003</td>
	                    <td><i>1.3800</i>2014.10.16</td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	                <tr>
	                    <td>
		                    <em>中邮核心优势</em>590003
	                    </td>
	                    <td>
	                    	<i>1.3800</i>
	                    	2014.10.16
	                    </td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	               	<tr>
	                    <td><em>中邮核心优势</em>590003</td>
	                    <td><i>1.3800</i>2014.10.16</td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	                <tr>
	                    <td>
		                    <em>中邮核心优势</em>590003
	                    </td>
	                    <td>
	                    	<i>1.3800</i>
	                    	2014.10.16
	                    </td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	               	<tr>
	                    <td><em>中邮核心优势</em>590003</td>
	                    <td><i>1.3800</i>2014.10.16</td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	                <tr>
	                    <td>
		                    <em>中邮核心优势</em>590003
	                    </td>
	                    <td>
	                    	<i>1.3800</i>
	                    	2014.10.16
	                    </td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	               	<tr>
	                    <td><em>中邮核心优势</em>590003</td>
	                    <td><i>1.3800</i>2014.10.16</td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	                <tr>
	                    <td>
		                    <em>中邮核心优势</em>590003
	                    </td>
	                    <td>
	                    	<i>1.3800</i>
	                    	2014.10.16
	                    </td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	               	<tr>
	                    <td><em>中邮核心优势</em>590003</td>
	                    <td><i>1.3800</i>2014.10.16</td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	            </tbody>
	        </table>
		</section>
		<!-- 底部导航 -->
		<footer class="footer">
				<a href="fund.php">基金</a>
				<a href="info_management.php">资管</a>
				<a href="news_list.php">研发资讯</a>
				<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/touch.js"></script>
	<script src="../js/common.js"></script>
	<script src="../js/fx.js"></script>
	 
</body>
</html>