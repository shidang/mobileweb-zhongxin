<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>在线客服首页</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/swiper.3.1.7.min.css">
	<link rel="stylesheet" href="../css/animate.min.css">
	<link rel="stylesheet" type="text/css" href="../css/common.css" />
	<link rel="stylesheet" type="text/css" href="../css/page.css" />
	<style type="text/css">
		html, body {
		 	margin: 0 auto;
		 	background: #f2f2f2;
		}
	</style>
</head>
<body>
	<div class="online">
		<!-- 首页头部 -->
		<header class="online-head">
			<a href="index.php"></a>
			<h1>在线客服</h1>
		</header>
		<section>
		<!-- 基金大图滚动和内容模块 -->
			<div class="online-pic">
			<!-- 大图滚动 -->
				<div class="swiper-container">
				    <div class="swiper-wrapper">
				        <div class="swiper-slide"><img src="../images/demo/fund13.png" alt=""></div>
				        <div class="swiper-slide"><img src="../images/demo/fund12.png" alt=""></div>
				        <div class="swiper-slide"><img src="../images/demo/fund11.png" alt=""></div>
				    </div>
			    	<div class="swiper-pagination "></div>
				</div>
			</div>
			<!-- 内容模块 -->
			<div class="online-main clearfix">
				<ul>
					<li> <a href="account_consultation.php"></a>开户咨询 </li>
					<li> <a href="fund.php"></a>基    金 </li>
					<li> <a href="info_management.php"></a>资    管</li>
					<li> <a href="message_push.php"></a>消息推送 </li>
				</ul>
			</div>
		</section>
		<!-- 底部导航 -->
		<footer class="common-footer">
				<a href="fund.php">基金</a>
				<a href="info_management.php">资管</a>
				<a href="news_list.php">研发资讯</a>
				<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/common.js"></script>
	<script src="../js/swiper.3.1.7.jquery.min.js"></script>
	<script src="../js/touch.js"></script>
	<script src="../js/fx.js"></script>
	<script src="../js/swiper.animate1.0.2.min.js"></script>
	<script type="text/javascript">
		// 大图滚动swiper
		var mySwiper = new Swiper ('.swiper-container', {
    		direction: 'horizontal',
    		loop: true,
    		autoplay : 3000,
    
   			 // 如果需要分页器
   		 	pagination: '.swiper-pagination',
  		})  
	
	</script>
</body>
</html>