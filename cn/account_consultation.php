<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <title>中信建投(开户咨询)</title>
    <link rel="shortcut icon" type="image/x-icon" href="../images/favicon.ico">
    <link rel="stylesheet" type="text/css" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" href="../css/common.css" />
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <style type="text/css">
        html {
            max-width: 640px;
            margin: 0 auto;
            background: #fff;
        }
    </style>
</head>
<body>
    <div class="account-consultation">
        <header>
            开户咨询
            <a href="online_service.php"><span class="left"></span></a>
        </header>
        <div class="remind">
            <span>
                您好，客服不在线。您可以通过机器人自助服
                务，或 <a href="">留言</a> 或者请求 <a href="">客服服务</a>
            </span>
        </div>
        <div class="account-nav">
            <span class="select-nav">商品期货</span>
            <span>金融期货</span>
        </div>
        <div class="account-con">
            <span class="select-con">
                1. 法人申请金融期货交易编码需要携带哪些资料？<br>
                2. 自然人申请金融期货交易编码适当性综合...？<br>
                3. 自然人投资者申请开立金融期货交易编...？
            </span>
            <span>
                1. 自然人投资者申请开立金融期货交易编...？<br>
                2. 法人申请金融期货交易编码需要携带哪些资料？<br>
                3. 自然人申请金融期货交易编码适当性综合...？
            </span>
        </div>
        <a class="btn" target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=511793598&site=qq&menu=yes">在线聊天</a>
        <footer class="common-footer">
            <a href="fund.php">基金</a>
            <a href="info_management.php">资管</a>
            <a href="news_list.php">研发资讯</a>
            <a href="online_service.php">在线客服</a>
        </footer> 
    </div>
    <script type="text/javascript" src="../js/zepto.min.js"></script>
    <script type="text/javascript" src="../js/touch.js"></script> 
    <script type="text/javascript" src="../js/common.js"></script>
    <script type="text/javascript">
        $(".account-nav span").tap(function(){
            var index = $(this).index(); 
            $(this).addClass("select-nav").siblings().removeClass();
            $(".account-con span").eq(index).addClass("select-con").siblings().removeClass();;
        })
    </script>
</body>
</html>