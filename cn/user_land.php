<?php 
    session_start();
?>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<title>用户登录</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/common.css">
	<link rel="stylesheet" href="../css/cn_page.css">
</head>
<body>
	<div class="user-land">
		<header class="common-header user-land-arrow"><a class="user-arrow" href="index.php"></a><em>登录</em><a href="registered.php">注册</a></header>
		<form action="" id="trad">
			<div><input type="text" name="username" value="" placeholder="请输入用户名" class="names"></div>
			<div><input type="password" name="password" value="" placeholder="请输入交易密码" class="password"></div>
			<div class="clearfix"><input type="text" name="" value="" placeholder="验证码" class="code"><span></span></div>
			<div class="remember clearfix"><strong></strong><span>记住用户名</span><em>忘记密码</em></div>
			<div><input type="submit" value="登录" id="sub"></div>
		</form>
		<footer class="common-footer">
			<a href="fund.php">基金</a>
			<a href="info_management.php">资管</a>
			<a href="news_list.php">研发资讯</a>
			<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/touch.js"></script>
	<script type="text/javascript">
		$("#trad").on("submit", function(e){
            e.preventDefault();
            $.post("../php/login_ajax.php", $("form").serializeArray(), function(response){
            	var js = JSON.parse(response);	
                if(!js.state){
                    alert("你的用户名或密码错误")                
                } else {
                	window.location.href = "index.php";
                }
            })      
        })
	</script>
</body>
</html>