<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>资管产品详情页</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/fangda_info.css">
	<link rel="stylesheet" href="../css/com.css">

</head>
<body>
	<div class="fund-fangda">
		<!-- 方达资管产品详情页头部 -->
		<header class="fund-head">
			<a href="info_management.php"></a>
			<h1>
				<strong>方达资管</strong>
				<span>900013</span>
			</h1>
			<a href="fangda_load.php">购买</a>
		</header>
		<section>
		<!-- 内容模块 -->
			
			<div class="fangda-table">
				<ul>
					<li><strong>产品名称</strong><em>方达资管</em></li>
					<li><strong>产品类型</strong><em>一对一资管产品</em></li>
					<li><strong>成立日期</strong><em>2014-03-20</em></li>
					<li><strong>单位净值</strong><em>1.2733</em></li>
					
				</ul>
			</div>
			<div class="word">
				<h2>产品简介</h2>
				<p>公司于1996年成立，注册资本61623.5万，实际控制人为云南省人民政府国有资产监督管理委员会，主营化工及化纤材料、纯碱、氯化铵、水泥、氧气产品生产和销售，机械，机电，五金，金属材料的批发、零售代购代销；汽车客货运输、汽车维修；经营企业自产产品及技术的出口业务，进口本企业所需的原辅材料、仪器仪表、机械设备及技术。</p>
				<p>公司于1996年7月在上海交易所上市，A股代码600725。</p>
			</div>
			<div class="fangda-pic">
				<nav class="clearfix">
					<a href="" class="fd-pic">分时</a>
					<a href="">五日</a>
					<a href="">日K</a>
					<a href="">周K</a>
					<a href="">月K</a>
				</nav>
				<img src="../images/demo/fund15.png" alt="">
			</div>
			<table class="date-data">
	            <thead>
	                <tr>
	                    <th>日 期</th>
	                    <th>单位净值</th>
	                    <th>累计净值</th>
	                </tr>     
	            </thead>
	            <tbody>
	                
	                 <tr>
	                    <td>2014.10.10</td>
	                    <td><em>1.005</em></td>
	                    <td ><span class="res">0.54</span></td>
	                </tr>
	               <tr>
	                    <td>2014.10.10</td>
	                    <td><em>1.005</em></td>
	                    <td ><span class="res">0.54</span></td>
	                </tr>
	                 <tr>
	                    <td>2014.10.10</td>
	                    <td><em>1.005</em></td>
	                    <td ><span class="res">0.54</span></td>
	                </tr>
	                 <tr>
	                    <td>2014.10.10</td>
	                    <td><em>1.005</em></td>
	                    <td ><span class="res">0.54</span></td>
	                </tr>
	            </tbody>
	        </table>
		</section>
		<!-- 底部导航 -->
		<footer class="footer">
				<a href="fund.php">基金</a>
				<a href="info_management.php">资管</a>
				<a href="news_list.php">研发资讯</a>
				<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/touch.js"></script>
	<script src="../js/common.js"></script>
	<script src="../js/fx.js"></script>
	 <script type="text/javascript">
        $(".nav-con").tap(function() {
            $(this).parent().parent().animate({
                "height" : "25rem",
                "box-shadow" : "none",
            }, 1000);
            $(this).parent().children('.left').css({
                "background" : "url(../images/arrow_down.png) 0 0 no-repeat",
                "background-size" : "contain"
            })
            $(this).parent().parent().siblings().animate({
                "height" : "3.42rem",
                "box-shadow" : "0 0.1rem 0 0 #d12626"
            }, 1000).children('.nav-li').children('.left').css({
                "background" : "url(../images/con-arrow.png) 0 0 no-repeat",
                "background-size" : "contain"
            });
        })
    </script>
</body>
</html>