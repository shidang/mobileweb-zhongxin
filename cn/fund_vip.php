<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>专户净值</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/fund_vip.css">
	<link rel="stylesheet" href="../css/com.css">
</head>
<body>
	<div class="fund-vip">
		<!-- 基金净值头部 -->
		<header class="fund-head">
			<a href="fund.php"></a>
			<h1>
				专户净值
			</h1>
			
		</header>
		<section>
		<!-- 内容模块 -->
			<!-- 内容导航模块 -->
			<nav class="vip-nav">
				<input type="text" value="输入股票名称/代码添加自选股">
			</nav>
			<table class="date-data clearfix">
	            <thead>
	                <tr>
	                    <th >基金名称</th>
	                    <th>最新净值</th>
	                    <th class="head-con">日涨跌幅</th>
	                </tr>     
	            </thead>
	            <tbody>
	                
	                <tr>
	                    <td>
		                    <em>中邮核心优势</em>590003
	                    </td>
	                    <td>
	                    	<i>1.3800</i>
	                    	2014.10.16
	                    </td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	               	<tr>
	                    <td><em>中邮核心优势</em>590003</td>
	                    <td><i>1.3800</i>2014.10.16</td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	                <tr>
	                    <td>
		                    <em>中邮核心优势</em>590003
	                    </td>
	                    <td>
	                    	<i>1.3800</i>
	                    	2014.10.16
	                    </td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	               	<tr>
	                    <td><em>中邮核心优势</em>590003</td>
	                    <td><i>1.3800</i>2014.10.16</td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	                <tr>
	                    <td>
		                    <em>中邮核心优势</em>590003
	                    </td>
	                    <td>
	                    	<i>1.3800</i>
	                    	2014.10.16
	                    </td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	               	<tr>
	                    <td><em>中邮核心优势</em>590003</td>
	                    <td><i>1.3800</i>2014.10.16</td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	                <tr>
	                    <td>
		                    <em>中邮核心优势</em>590003
	                    </td>
	                    <td>
	                    	<i>1.3800</i>
	                    	2014.10.16
	                    </td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	               	<tr>
	                    <td><em>中邮核心优势</em>590003</td>
	                    <td><i>1.3800</i>2014.10.16</td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	                <tr>
	                    <td>
		                    <em>中邮核心优势</em>590003
	                    </td>
	                    <td>
	                    	<i>1.3800</i>
	                    	2014.10.16
	                    </td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	               	<tr>
	                    <td><em>中邮核心优势</em>590003</td>
	                    <td><i>1.3800</i>2014.10.16</td>
	                    <td ><span class="res">+0.54%</span></td>
	                </tr>
	            </tbody>
	        </table>
		</section>
		<!-- 底部导航 -->
		<footer class="footer">
				<a href="fund.php">基金</a>
				<a href="info_management.php">资管</a>
				<a href="news_list.php">研发资讯</a>
				<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/touch.js"></script>
	<script src="../js/common.js"></script>
	<script src="../js/fx.js"></script>
	 
</body>
</html>