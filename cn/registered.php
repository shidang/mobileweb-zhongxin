<?php
    include("../php/include.php");
?>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<title>注册-游客</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/common.css">
	<link rel="stylesheet" href="../css/cn_page.css">
</head>
<body>
	<div class="user-bound register">
		<header class="common-header register-head"><a class="user-arrow" href="user_land.php"></a><em>注册</em></header>
		<div class="numbers"><h2><a href="registered_tour.php"class="click-show jiaoyi"><strong ></strong>交易账户</a><a href="registered.php"class="colors click-show youke"><strong class="start"></strong>游客</a></h2></div>
		<form action="" id="trad">
			<div><input type="text" name="username" value="" placeholder="请输入中文用户名" class="names"></div>
			<div><input type="text" name="mail" value="" placeholder="请输入邮箱" class="names"></div>
			<div><input type="text" name="tel" value="" placeholder="请输入手机号" class="names"></div>
			<div class="clearfix code"><input type="text" name="" value="" placeholder="验证码" class=""><span>发送验证码</span></div>
			<div><input type="password" name="password" value="" placeholder="设置密码" class="password names"></div>
			<div><input type="password" value="" placeholder="确认密码" class="password names"></div>
			<ul><li>想获取期货相关信息</li></ul>
			<div><input type="submit" value="登录" id="trad-btn"></div>
		</form>
		<footer class="common-footer">
			<a href="fund.php">基金</a>
			<a href="info_management.php">资管</a>
			<a href="news_list.php">研发资讯</a>
			<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/touch.js"></script>
	<script src="../js/selector.js"></script>
	<script src="../js/re.js"></script>
	<script>
		// tabFun.c();

		re.a("#trad div:nth-child(1) input","请输入中文用户名",reg.name1);
		re.a("#trad div:nth-child(2) input","请输入正确邮箱格式",reg.email);
		re.a("#trad div:nth-child(3) input","请输入11位手机号",reg.phone);
		re.a("#trad div:nth-child(5) input","请用数字，字母，下划线",reg.pass);

	</script>
</body>
</html>