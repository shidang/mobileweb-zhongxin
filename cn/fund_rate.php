<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>基金费率</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/com.css">

</head>
<body>
	<div class="fund-rate">
		<!-- 基金净值头部 -->
		<header class="fund-head">
			<a href="fund_hot.php"></a>
			<h1>
				<strong>汇添富民营活力</strong>
				<span>470009</span>
			</h1>
			<a href=""></a>
		</header>
		<section>
		<!-- 内容模块 -->
			<!-- 内容导航模块 -->
			<nav class="rate-nav">
				<ul>
					<li> <a href="fund_details.php">基金概况</a> </li>
					<li> <a href="fund_up_down.php">涨跌幅</a> </li>
					<li> <a href="fund_value.php">基金净值</a></li>
					<li> <a href="fund_rate.php">基金费率 </a></li>
				</ul>
			</nav>
			<div class="rate-main">
				<dl>
					<dt>管理费率（年）1.5%托管费率（年）0.25%</dt>
					<dd class="clearfix">
						<span>购买金额（m）</span>
						<span>认购费率 %</span>
					</dd>
				</dl>
				<dl>
					<dt>申购费率</dt>
					<dd class="clearfix">
						<span>购买金额（m）</span>
						<span>申购费率 %</span>
						<em>网上直销申购费率%</em>
					</dd>
				</dl>
				<dl>
					<dt>赎回费率</dt>
					<dd class="clearfix">
						<span>持有期限（T）</span>
						<span>场外赎回费率 %</span>
					</dd>
				</dl>
				<dl>
					<dt>本基金的场内赎回费率为固定值0.5%</dt>
					<dd class="clearfix">
						<p>以上信息仅作参考，具体内容请以该基金最新的份额发售公告、基金合同、招募说明书为准</p>
					</dd>
				</dl>
			</div>
		</section>
		<!-- 底部导航 -->
		<footer class="footer">
				<a href="fund.php">基金</a>
				<a href="info_management.php">资管</a>
				<a href="news_list.php">研发资讯</a>
				<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/touch.js"></script>
	<script src="../js/common.js"></script>
	<script src="../js/fx.js"></script>
	 
</body>
</html>