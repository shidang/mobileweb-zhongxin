<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>基金详情页</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/com.css">

</head>
<body>
	<div class="fund-details">
		<!-- 基金首页头部 -->
		<header class="fund-head">
			<a href="fund.php"></a>
			<h1>
				<strong>汇添富民营活力</strong>
				<span>470009</span>
			</h1>
			<a href=""></a>
		</header>
		<section>
		<!-- 内容模块 -->
			<!-- 内容导航模块 -->
			<nav class="details-nav">
				<ul>
					<li> <a href="fund_details.php">基金概况</a> </li>
					<li> <a href="fund_up_down.php">涨跌幅</a> </li>
					<li> <a href="fund_value.php">基金净值</a></li>
					<li> <a href="fund_rate.php">基金费率 </a></li>
				</ul>
			</nav>
			<div class="details-table">
				<ul>
					<li><strong>基金名称</strong><em>国金通用鑫盈货币市场证券</em></li>
					<li><strong>基金类型</strong><em>货币型基金</em></li>
					<li><strong>单位面值</strong><em>1</em></li>
					<li><strong>基金经理</strong><em>徐艳芳、滕祖光</em></li>
					<li><strong>基金管理人</strong><em>国金通用基金</em></li>
					<li><strong>基金托管人</strong><em>兴业银行</em></li>
				</ul>
			</div>
			<div class="con">
	            <ul>
	                <li class="nav-li">
	                    <span class="left"></span>
	                    <span class="nav-con">基金公告</span>
	                    <a href="fund_infomation.php"><span class="right"></span></a>
	                </li>
	                <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                 <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                 <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	            </ul>
	            <ul>
	                <li class="nav-li">
	                    <span class="left"></span>
	                    <span class="nav-con">基金公告</span>
	                    <a href="fund_infomation.php"><span class="right"></span></a>
	                </li>
	                <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                 <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                 <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	            </ul>
	            <ul>
	                <li class="nav-li">
	                    <span class="left"></span>
	                    <span class="nav-con">基金公告</span>
	                    <a href="fund_infomation.php"><span class="right"></span></a>
	                </li>
	                <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href="fund_content.php"><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                 <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                 <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	                <li>
	                    <a href=""><h2 class="beyond">金元惠理保本混合型证券投资基金保到本金</h2>
	                    <span class="beyond">08:14</span></a>     
	                </li>
	            </ul>
	        </div>
		</section>
		<!-- 底部导航 -->
		<footer class="footer">
				<a href="fund.php">基金</a>
				<a href="account.php">资管</a>
				<a href="news_list.php">研发资讯</a>
				<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/touch.js"></script>
	<script src="../js/common.js"></script>
	<script src="../js/fx.js"></script>
	<script type="text/javascript"></script>
</body>
</html>