<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>基金首页</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/swiper.min.css">
	<link rel="stylesheet" href="../css/animate.min.css">
	<link rel="stylesheet" href="../css/com.css">

</head>
<body>
	<div class="fund">
		<!-- 基金首页头部 -->
		<header class="fund-head">
			<a href="index.php"></a>
			<h1>
				基 金
			</h1>
			<a href=""></a>
		</header>
		<section>
		<!-- 基金大图滚动和内容模块 -->
			<div class="fund-pic">
			<!-- 大图滚动 -->
				<div class="swiper-container">
				    <div class="swiper-wrapper">
				        <div class="swiper-slide"><img src="../images/demo/fund13.png" alt=""></div>
				        <div class="swiper-slide"><img src="../images/demo/fund12.png" alt=""></div>
				        <div class="swiper-slide"><img src="../images/demo/fund11.png" alt=""></div>
				    </div>
			    	<div class="swiper-pagination "></div>
				</div>
			</div>
			<!-- 内容模块 -->
			<div class="fund-main clearfix">
				<ul class="clearfix">
					<li> <a href="fund_hot.php"></a>热销基金 </li>
					<li> <a href="fund_value.php"></a>基金净值 </li>
					<li> <a href="fund_vip.php"></a>专户基金</li>
					<li> <a href="fund_me.php"></a>我的自选 </li>
					<li> <a href="fund_infomation.php"></a>基金资讯</li>
				</ul>
			</div>
		</section>
		<!-- 底部导航 -->
		<footer class="footer">
				<a href="fund.php">基金</a>
				<a href="info_management.php">资管</a>
				<a href="news_list.php">研发资讯</a>
				<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/touch.js"></script>
	<script src="../js/common.js"></script>
	<script src="../js/swiper.min.js"></script>
	<script src="../js/fx.js"></script>
	<script src="../js/animate.min.js"></script>

	<script type="text/javascript">
		// 大图滚动swiper
		var mySwiper = new Swiper ('.swiper-container', {
    		direction: 'horizontal',
    		loop: true,
    		autoplay : 3000,
    
   			 // 如果需要分页器
   		 	pagination: '.swiper-pagination',
  		})  
	
	</script>
</body>
</html>