<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>基金详情页</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" type="text/css" href="../css/com.css" /> 
</head>
<body>
	<div class="fund-hot">
		<!-- 基金净值头部 -->
		<header class="fund-head">
			<a href="fund.php"></a>
			<h1>
				热销基金
			</h1>
			<a href=""></a>
		</header>
				
		<section>
		<!-- 内容模块 -->
			<table class="date-data">
	            <thead>
	                <tr>
	                    <th >名称代码</th>
	                    <th>基金类型</th>
	                    <th class="head-con">累计净值</th>
	                </tr>     
	            </thead>
	            <tbody>
	                
	               	<tr>
	                    <td>
		                    <em>中邮核心优势</em><i class="fo">590003</i>
	                    </td>
	                    <td>
	                    	股债平衡型
	                    </td>
	                    <td class="res">
	                    	1.3800
	                    </td>
	                </tr>
	               	<tr>
	                    <td>
		                    <em>中邮核心优势</em><i class="fo">590003</i>
	                    </td>
	                    <td>
	                    	股债平衡型
	                    </td>
	                    <td class="res">
	                    	1.3800
	                    </td>
	                </tr>
	                <tr>
	                    <td>
		                    <em>中邮核心优势</em><i class="fo">590003</i>
	                    </td>
	                    <td>
	                    	股债平衡型
	                    </td>
	                    <td class="res">
	                    	1.3800
	                    </td>
	                </tr>
	               	<tr>
	                    <td>
		                    <em>中邮核心优势</em><i class="fo">590003</i>
	                    </td>
	                    <td>
	                    	股债平衡型
	                    </td>
	                    <td class="res">
	                    	1.3800
	                    </td>
	                </tr>
	                <tr>
	                    <td>
		                    <em>中邮核心优势</em><i class="fo">590003</i>
	                    </td>
	                    <td>
	                    	股债平衡型
	                    </td>
	                    <td class="res">
	                    	1.3800
	                    </td>
	                </tr>
	               	<tr>
	                    <td>
		                    <em>中邮核心优势</em><i class="fo">590003</i>
	                    </td>
	                    <td>
	                    	股债平衡型
	                    </td>
	                    <td class="res">
	                    	1.3800
	                    </td>
	                </tr>
	                <tr>
	                    <td>
		                    <em>中邮核心优势</em><i class="fo">590003</i>
	                    </td>
	                    <td>
	                    	股债平衡型
	                    </td>
	                    <td class="res">
	                    	1.3800
	                    </td>
	                </tr>
	               	<tr>
	                    <td>
		                    <em>中邮核心优势</em><i class="fo">590003</i>
	                    </td>
	                    <td>
	                    	股债平衡型
	                    </td>
	                    <td class="res">
	                    	1.3800
	                    </td>
	                </tr>
	                <tr>
	                    <td>
		                    <em>中邮核心优势</em><i class="fo">590003</i>
	                    </td>
	                    <td>
	                    	股债平衡型
	                    </td>
	                    <td class="res">
	                    	1.3800
	                    </td>
	                </tr>
	               	<tr>
	                    <td>
		                    <em>中邮核心优势</em><i class="fo">590003</i>
	                    </td>
	                    <td>
	                    	股债平衡型
	                    </td>
	                    <td class="res">
	                    	1.3800
	                    </td>
	                </tr>
	            </tbody>
	        </table>
		</section>
		<!-- 底部导航 -->
		<footer class="footer">
				<a href="fund.php">基金</a>
				<a href="info_management.php">资管</a>
				<a href="news_list.php">研发资讯</a>
				<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/touch.js"></script>
	<script src="../js/common.js"></script>
	<script src="../js/fx.js"></script>
	 
</body>
</html>