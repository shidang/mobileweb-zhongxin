<?php 
    include("../php/include.php");
 ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>中信建投</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" href="../css/animate.min.css" />  
    <link rel="stylesheet" type="text/css" href="../css/common.css" /> 
    <link rel="stylesheet" type="text/css" href="../css/page.css" />  
    <style type="text/css">
        html {
            max-width: 640px;
            margin: 0 auto;
            background: #fff;
        }
    </style>
</head>
<body>
    <div class="message-push">
        <header>
            消息推送
            <a href="online_service.php"></a>
        </header>
        <div class="con">
            <ul>
                <li class="nav-li">
                    <span class="left"></span>
                    <span class="nav-con">基金 1</span>
                    <a href=""><span class="right"></span></a>
                </li>
                <?php 
                    $sql = mysql_query("SELECT * FROM news LIMIT 0, 5");
                    while($row = mysql_fetch_assoc($sql)) {
                ?>   
                <li>
                    <a href="news_content.php?aid=<?php echo $row['id'];?>"><h2 class="beyond"><?php echo $row["title"]; ?></h2>
                    <span class="beyond"><?php echo date("H:m", strtotime($row["date"])); ?></span></a>  
                </li>
                <?php } ?>
            </ul>
            <ul>
                <li class="nav-li">
                    <span class="left"></span>
                    <span class="nav-con">基金 2</span>
                    <a href=""><span class="right"></span></a>
                </li>
                <?php 
                    $sql = mysql_query("SELECT * FROM news LIMIT 5, 5");
                    while($row = mysql_fetch_assoc($sql)) {
                ?>   
                <li>
                    <a href="news_content.php?aid=<?php echo $row['id'];?>"><h2 class="beyond"><?php echo $row["title"]; ?></h2>
                    <span class="beyond"><?php echo date("H:m", strtotime($row["date"])); ?></span></a>  
                </li>
                <?php } ?>
            </ul>
            <ul>
                <li class="nav-li">
                    <span class="left"></span>
                    <span class="nav-con">基金 3</span>
                    <a href=""><span class="right"></span></a>
                </li>
                <?php 
                    $sql = mysql_query("SELECT * FROM news LIMIT 3, 5");
                    while($row = mysql_fetch_assoc($sql)) {
                ?>   
                <li>
                    <a href="news_content.php?aid=<?php echo $row['id'];?>"><h2 class="beyond"><?php echo $row["title"]; ?></h2>
                    <span class="beyond"><?php echo date("H:m", strtotime($row["date"])); ?></span></a>  
                </li>
                <?php } ?>
            </ul>
        </div>
        <footer class="common-footer">
            <a href="fund.php">基金</a>
            <a href="info_management.php">资管</a>
            <a href="news_list.php">研发资讯</a>
            <a href="online_service.php">在线客服</a>
        </footer>
    </div>
    <script src="../js/zepto.min.js" type="text/javascript"></script>
    <script src="../js/selector.js" type="text/javascript"></script>
    <script src="../js/touch.js" type="text/javascript"></script>
    <script src="../js/fx.js" type="text/javascript"></script>
    <script src="../js/fx_methods.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/common.js"> </script>
</body>
</html>