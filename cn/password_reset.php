<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<title>我的账户</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/common.css">
	<link rel="stylesheet" href="../css/cn_page.css">
</head>
<body>
	<div class="user-bound pass-reset">
		<header class="common-header"><em>我的账户</em><a href="index.php">退出</a></header>
		<div class="numbers"><h2><b class="colors"><strong class="start"></strong>密码重置</b></h2></div>
		<form action="" id="password-reset">
			<div><input type="text" name="username" value="" placeholder="请输入账号" class="names"></div>
			<div><input type="password " name="password" value="" placeholder="请输入密码" class="password names"></div>
			<div><input type="password " value="" placeholder="再输入密码" class="password names"></div>
			<div class="clearfix code"><input type="text" name="" value="" placeholder="验证码" class=""><span>发送验证码</span></div>
			<div><input type="submit" value="确定" class="sub-btn"></div>
		</form>
		<footer class="common-footer">
			<a href="fund.php">基金</a>
			<a href="info_management.php">资管</a>
			<a href="news_list.php">研发资讯</a>
			<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/touch.js"></script>
	<script src="../js/re.js"></script>
	<script>
		re.a("#password-reset div:nth-child(1) input","请用数字，字母，下划线",reg.pass);
		re.a("#password-reset div:nth-child(2) input","请用数字，字母，下划线",reg.pass);

		$("#password-reset").on("submit", function(e){
			console.log(11111)
			e.preventDefault();
            $.post("../php/pass_ajax.php", $("form").serializeArray(), function(response){
            	var js = JSON.parse(response);	
                if(!js.state){
                    alert("用户不存在")                
                } else {
                	alert("修改成功") 
                }
            })      
        })

	</script>
</body>
</html>