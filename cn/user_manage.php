<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<title>资管账号</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/common.css">
	<link rel="stylesheet" href="../css/cn_page.css">
</head>
<body>
	<div class="user-bound">
		<header class="common-header"><em>绑定</em></header>
		<div class="numbers"><h2><span class="colors"><strong ></strong>期货账号</span><em><strong class="startSd"></strong>资管账号</em></h2></div>
		<form action="" id="data">
			<div><input type="text" name="account" value="" placeholder="请输入账号" class="names"></div>
			<div><input type="text" name="name" value="" placeholder="请输入姓名" class="names"></div>
			<div><input type="text" name="card" value="" placeholder="请输入身份证" class="names"></div>
			<div><input type="text" name="tel" value="" placeholder="请输入手机号" class="names"></div>
			<div><input type="password " name="password" value="" placeholder="请输入密码" class="password names"></div>
			<div class="clearfix code"><input type="text" name="" value="" placeholder="验证码" class=""><span>发送验证码</span></div>
			<div class="remember clearfix"><span>还没有账号,</span><a href="">立即开户</a></div>
			<div><input type="submit" name="btn" value="绑定"></div>
		</form>
		<footer class="common-footer">
			<a href="fund.php">基金</a>
			<a href="info_management.php">资管</a>
			<a href="news_list.php">研发资讯</a>
			<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/touch.js"></script>
	<script>
		$("#data").on("submit", function(e){
			console.log(11111)
			e.preventDefault();
            $.post("../php/band_ajax.php", $("form").serializeArray(), function(response){
            	var js = JSON.parse(response);	
                if(!js.state){
                    alert("用户不存在");                
                } else {
                	alert("绑定成功"); 
                }
            })      
        })
	</script>
</body>
</html>