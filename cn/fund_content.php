<?php 
    include("../php/include.php");
    $aid = $_GET["aid"];
    $sql = mysql_query("SELECT * FROM news WHERE id=$aid");
    $row = mysql_fetch_assoc($sql);
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <title>中信建投(基金公告)</title>
    <link rel="shortcut icon" type="image/x-icon" href="../images/favicon.ico">
    <link rel="stylesheet" type="text/css" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" href="../css/common.css" />
    <link rel="stylesheet" type="text/css" href="../css/page.css" />
    <style type="text/css">
        html {
            max-width: 640px;
            min-height: 120%;
            margin: 0 auto;
            background: #fff;
        }
    </style>
</head>
<body>
    <div class="fund-content">
        <header>
            基金公告
            <a href="fund_infomation.php"><span class="left"></span></a>
            <a href=""><span class="share"></span></a>
        </header>
        <article>
            <h2><?php echo $row["title"]; ?></h2>
            <div class="article-data">
                <span>来源：<?php echo $row["from"]; ?>    &nbsp<?php echo date("Y-m-d", strtotime($row["date"]));?>   &nbsp<?php echo date("H:m", strtotime($row["date"])); ?></span>
            </div>
            <div class="con">
                <p>
                    <?php echo $row["content"]; ?>
                </p>
            </div>
        </article>
        <footer class="common-footer">
            <a href="fund.php">基金</a>
            <a href="info_management.php">资管</a>
            <a href="news_list.php">研发资讯</a>
            <a href="online_service.php">在线客服</a>
        </footer> 
    </div>
    <script type="text/javascript" src="../js/zepto.min.js"></script>
    <script type="text/javascript" src="../js/touch.js"></script> 
    <script type="text/javascript" src="../js/common.js"></script>
    <script type="text/javascript">

    </script>
</body>
</html>