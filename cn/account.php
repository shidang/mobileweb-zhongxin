<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<title>我的账户---</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/common.css">
	<link rel="stylesheet" href="../css/cn_page.css">
</head>
<body>
	<div class="user-bound data-reset account">
		<header class="common-header"><em>我的账户</em><a href="">退出</a></header>
		<nav>
			<div><a href="data_reset.php">资料修改</a></div>
			<div><a href="user_bound.php">资管</a></div>
			<div><a href="user_manage.php">基金</a></div>
			<div><a href="password_reset.php">密码重置</a></div>
			<div><a href="fund_net.php">专户净值查询</a></div>
		</nav>
		<footer class="common-footer">
			<a href="fund.php">基金</a>
			<a href="info_management.php">资管</a>
			<a href="news_list.php">研发资讯</a>
			<a href="online_service.php">在线客服</a>
		</footer>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script src="../js/touch.js"></script>
</body>
</html>