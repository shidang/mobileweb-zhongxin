  var mySwiper = new Swiper ('.swiper-container', {
    direction: 'horizontal',
    loop: true,
    autoplay : 2000,
    // 如果需要分页器
    pagination: '.swiper-pagination',
  })  

  var tapFun = {
  	a: function() {
  		var startX;
  		var startY;
  		var endX;
  		var endY;
  		$(".index")[0].addEventListener("touchstart",function(event){
			var event = window.event || event;
			if(event.touches.length == 1) {
				startX = event.touches[0].clientX;
				startY = event.touches[0].clientY;
			}
		},false);
		$(".index")[0].addEventListener("touchmove",function(event){
			var event = window.event || event;
			if(event.touches.length == 1) {
				endX = event.touches[0].clientX;
				endY = event.touches[0].clientY;
			}
		},false);
		$(".index")[0].addEventListener("touchend",function(event){
			
			if( endX - startX > 100) {
				$(".side").animate({
					"left": 0
				}, 500)
				$(".center").animate({
					"left": "80%"
				}, 500)

			}
			else if (endX - startX < -100){
				$(".side").animate({
					"left": "-80%"
				}, 500)
				$(".center").animate({
					"left": "0"
				}, 500)
			}		
		},false);
  	}
  }
