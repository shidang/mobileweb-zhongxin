/* 
* @Author:   SSD
* @Date:   2015-11-07 9:04
* @Last Modified by:   anchen
* @Last Modified time: 2015-11-07 11:09
*/

/*自适应字体*/
$(function(){
	$(window).resize(infinite);
	function infinite() {
		var htmlWidth = $('html').width();
		if (htmlWidth >= 640) {
			$("html").css({
				"font-size" : "24px"
			});
		} else {
			$("html").css({
				"font-size" :  24 / 640 * htmlWidth + "px"
			});
		}
	}infinite();
});

/*懒加载*/
function lan(load, loadSd){ 
    var strSd = "";
    $(window).scroll(function(){
        sdScroll = $(this).scrollTop();
        docSd = $("body").height();
        var scrollHeight = $(document).height();           //当前页面的总高度
    　　    var windowHeight = $(this).height(); 
       if(docSd >= 3000){
            $(load).css({display: "block"})
            $(loadSd).css({"display": "none"})
       }else {
            if(sdScroll + windowHeight >= scrollHeight){
                $.ajax({
                    url : "../php/list.json",
                    type : "get",
                    success: function(data){
                        strSd ="<li><h2 class='beyond'>" + data[0] + "</h2><span class='beyond'>08:14</span></li>";
                        strSd +="<li><h2 class='beyond'>" + data[1] + "</h2><span class='beyond'>08:14</span></li>";
                        strSd +="<li><h2 class='beyond'>" + data[2] + "</h2><span class='beyond'>08:14</span></li>";
                        strSd +="<li><h2 class='beyond'>" + data[3] + "</h2><span class='beyond'>08:14</span></li>"
                        setTimeout(timeload,1000);
                        function timeload() {
                            $("li:nth-last-child(1)").after(strSd);
                        };                         
                    }
                })                            
            }
       }                  
    })
}

/*下拉列表*/

 $(".nav-con").tap(function() {
    if($(this).parent().parent().hasClass("open")){
        $(this).parent().parent().removeClass("open");
        $(this).parent().parent().animate({
            "height" : "3.42rem",
            "box-shadow" : "0 0.1rem 0 0 #d12626",
        }, 500);
        $(this).parent().children('.left').css({
            "background" : "url(../images/con-arrow.png) 0 0 no-repeat",
            "background-size" : "contain"
        })
    }else{
        $(this).parent().parent().addClass("open");
        $(this).parent().parent().animate({
            "height" : "25.5rem",
            "box-shadow" : "none",
        }, 500);
        $(this).parent().children('.left').css({
            "background" : "url(../images/arrow_down.png) 0 0 no-repeat",
            "background-size" : "contain"
        })
        $(this).parent().parent().siblings().animate({
            "height" : "3.42rem",
            "box-shadow" : "0 0.1rem 0 0 #d12626"
        }, 500).children('.nav-li').children('.left').css({
            "background" : "url(../images/con-arrow.png) 0 0 no-repeat",
            "background-size" : "contain"
        });
        $(this).parent().parent().siblings().removeClass("open");
    }
})

$("input").attr({
    "maxlength": "20"
})