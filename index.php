<?php
	include("../php/include.php");
	session_start();
 ?>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<title>中信建投首页</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/swiper.3.1.7.min.css">
	<link rel="stylesheet" href="../css/reset.css">
	<link rel="stylesheet" href="../css/index.css">
	<link rel="stylesheet" href="../css/common.css">
</head>
<body>
	<div class="index">
		<div class="side" id="side">
		<?php  
				if(isset($_SESSION["uid"])) {
			?>
					<dl class="clearfix">
						<dt><img src="../images/demo/user.png" alt=""></dt>
						<dd>
							<div class="user"><?php echo $_SESSION["username"] ?></div>
							<div class="address">ShenZen</div>
						</dd>
					</dl>
					<a href="account.php" class="login">个人中心</a>
					<a href="message_push.php" class="info">通知</a>
			<?php

				} else {
			?>
					<dl class="clearfix">
						<dt><img src="../images/demo/user.png" alt=""></dt>
						<dd>
							<div class="user">user</div>
							<div class="address">ShenZhen</div>
						</dd>
					</dl>
					<a href="user_land.php" class="login">欢迎您,~请登陆</a>
					<a href="message_push.php" class="info">通知</a>
			<?php } ?>
		</div>
		<div class="center" id="center">
			<header class="common-header"><span class="index-user-detail"></span><em>中信建投</em>
			<?php  
				if(isset($_SESSION["uid"])) {
			?>
				<a href="##"></a>
			<?php

				} else {
			?>
				<a href="cn/user_land.php">登录</a>
			<?php } ?>
			</header>
			<div class="swiper-container">
			    <div class="swiper-wrapper">
			        <div class="swiper-slide"><a href="" ><img src="../images/demo/big-bg.png" alt="图片" ></a></div>
			        <div class="swiper-slide"><a href="" ><img src="../images/demo/big-bg.png" alt="图片" ></a></div>
			        <div class="swiper-slide"><a href="" ><img src="../images/demo/big-bg.png" alt="图片" ></a></div>
			    </div>
			    <!-- 如果需要分页器 -->
			    <div class="swiper-pagination"></div>
			</div>
			<nav class="nav clearfix">
				<a href="fund.php">基金</a>
				<a href="news_list.php">研发资讯</a>
				<a href="online_service.php">在线客服</a>
				<a href="fund_vip.php">专户净值</a>
				<a href="data_reset.php">资料修改</a>
				<a href="password_reset.php">密码修改</a>
				<a href="message_push.php">消息</a>
				<a href="fund_me.php">我的自选</a>
			</nav>
			<footer class="common-footer">
				<a href="fund.php">基金</a>
				<a href="info_management.php">资管</a>
				<a href="news_list.php">研发资讯</a>
				<a href="online_service.php">在线客服</a>
			</footer>
		</div>
	</div>
	<script src="../js/zepto.min.js"></script>
	<script type="text/javascript" src="../js/touch.js"></script>
	<script src="../js/swiper.3.1.7.jquery.min.js"></script>
	<script src="../js/fx.js"></script>
	<script src="../js/common.js" type="text/javascript"></script>
	<script src="../js/index.js"></script>
	<script>
		tapFun.a();
	</script>
</body>
</html>